/*
The function takes string only. Numbers are not allowed. 
Palindromes are not considered to be actual English words in current implementation.
They are any sequence of characters that read the same forward and backward.
So "ahA" will not be recognized as a palindrome, but "aha" will.
Single character and empty string are palindromes.
*/


"use strict";

isPalindrome( "aha" );

function isPalindrome( str ) {
	if ( typeof str !== "string" ) {
		console.log( "\nPlease, enter a string." )
	}
	else if ( str.length <= 1 ) {
		console.log( "\nThis is a palindrome." );
	}
	else {

		let strReversed = [...str].reverse().join("");

		if ( str === strReversed ) {
			console.log( "\nThis is a palindrome." );
		}
		else {
			console.log( "\nThis is not a palindrome." );
		}
	}
}