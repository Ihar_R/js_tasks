/*
The function may take any positive or negative number or zero as a second parameter.
In case of negative numbers or zero, letters in words are not replaced.
*/

"use strict";

replaceLetter("Sed accumsan felis. Ut at dolor quis odio consequat varius.", 5, "WORD");

function replaceLetter ( str1, num, str2 ) {

	let a = str1.split(" ");

	for (let i = 0; i < a.length; i++ ) {

		if ( a[i].length >= num ) {

			let b = Array.from(a[i]);
			b[num - 1] = str2;
			a[i] = b.join("");

		}
	}
	a = a.join(" ");
	console.log( "\n" + a );
}