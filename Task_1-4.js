"use strict";

let fs = require('fs');

readJSON("./ok.json");

function readJSON ( path ) {

  let file = require(path);
  let fileErrors = {};
  let okCounter = 0;


  if (file.hasOwnProperty("flag")) {
    if (typeof file.flag === "boolean") {
      okCounter++;
    }
    else {
      fileErrors.flag = "not boolean";
    }
  }
  else {
    fileErrors.flag = "no such property";
  }

  if (file.hasOwnProperty("myPromises")) {
    if (Array.isArray(file.myPromises)) {
      okCounter++;
    }
    else {
      fileErrors.myPromises = "not an array";
    }
  }
  else {
    fileErrors.myPromises = "no such property";
  }

  if (file.hasOwnProperty("element")) {
    if (file.element !== null && !Array.isArray(file.element) && typeof file.element === "object") {
      okCounter++;
    }
    else {
      fileErrors.element = "not an object";
    }
  }
  else {
    fileErrors.element = "no such property";
  }

  if (file.hasOwnProperty("screenshot")) {
    if (!file.screenshot && typeof file.screenshot === "object" ) {
      okCounter++;
    }
    else {
      fileErrors.screenshot = "not a null";
    }
  }
  else {
    fileErrors.screenshot = "no such property";
  }

  if (file.hasOwnProperty("elementText")) {
    if ( typeof file.elementText === "string" ) {
      okCounter++;
    }
    else {
      fileErrors.elementText = "not a string";
    }
  }
  else {
    fileErrors.elementText = "no such property";
  }

  if (file.hasOwnProperty("allElementsText")) {
    if ( typeof file.allElementsText === "string" ) {
      if (file.allElementsText.includes("const")) {
        okCounter++;
      }
      else {
        fileErrors.allElementsText = "doesn't contain 'const'";
      }
    }
    else {
      fileErrors.allElementsText = "not a string";
    }
  }
  else {
    fileErrors.allElementsText = "no such property";
  }

  if (file.hasOwnProperty("counter")) {
    if ( typeof file.counter === "number" ) {
      if (file.counter > 10) {
        okCounter++;
      }
      else {
        fileErrors.counter = "less then 11";
      }
    }
    else {
      fileErrors.counter = "not a number";
    }
  }
  else {
    fileErrors.counter = "no such property";
  }

  if (file.hasOwnProperty("config")) {
    if ( typeof file.config === "string" ) {
      if (file.config === "Common") {
        okCounter++;
      }
      else {
        fileErrors.config = "is not equal to 'Common'";
      }
    }
    else {
      fileErrors.config = "not a string";
    }
  }
  else {
    fileErrors.config = "no such property";
  }

  if (file.hasOwnProperty("const")) {
    if ( typeof file.const === "string" ) {
      if (file.const.toLowerCase() === "first") {
        okCounter++;
      }
      else {
        fileErrors.const = "is not equal to FiRst in any case";
      }
    }
    else {
      fileErrors.const = "not a string";
    }
  }
  else {
    fileErrors.const = "no such property";
  }

  if (file.hasOwnProperty("parameters")) {
    if ( Array.isArray(file.parameters)) {
      if (file.parameters.length === 8) {
        okCounter++;
      }
      else {
        fileErrors.parameters = "array length is not equal 8";
      }
    }
    else {
      fileErrors.parameters = "is not an array";
    }
  }
  else {
    fileErrors.parameters = "no such property";
  }

  if (file.hasOwnProperty("description")) {
    if ( typeof file.description === "string" ) {
      if ( file.description.length > 5 && file.description.length < 13 ) {
        okCounter++;
      }
      else {
        fileErrors.description = "string length is less than 6 or more than 12 ";
        console.log(file.description.length);
      }
    }
    else {
      fileErrors.description = "not a string";
    }
  }
  else {
    fileErrors.description = "no such property";
  }

  if(okCounter === 11){
    console.log("\nAll properties are OK");
  }
  else {
    fs.writeFile("mismatches.json", JSON.stringify(fileErrors), function (err) {
      if (err) throw err;
      console.log("\nFile created.")
    });
  }
}